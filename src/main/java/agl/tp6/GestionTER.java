package agl.tp6;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.time.LocalDateTime;

import agl.tp6.exception.AffectationImpossibleException;
import agl.tp6.exception.TailleInvalideException;
import agl.tp6.hongrois.Hongrois;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.format.DateTimeFormatter;


public class GestionTER {
	//attributs
	private ArrayList<Groupe> groupes;
	private ArrayList<Sujet> sujets;
	private ArrayList<String> profs;
	public Hongrois hongrois;

	//constructeur
	public GestionTER() {
	  this.groupes = new ArrayList<>();
	  this.sujets = new ArrayList<>();
	  this.profs = new ArrayList<>();
	}

	//getter/setter
	public ArrayList<Groupe> getGroupes() {
		return new ArrayList<>(this.groupes);
	}
	public void setGroupes(ArrayList<Groupe> groupes) {
		this.groupes = groupes;
	}
	public void setSujets(ArrayList<Sujet> sujets) {
		this.sujets = sujets;
	}
	public ArrayList<Sujet> getSujets() {
		return new ArrayList<>(this.sujets);
	}
	public void setProfs(ArrayList<String> profs) {
		this.profs = profs;
	}
	public ArrayList<String> getProfs() {
		return new ArrayList<>(this.profs);
	}
	public void addGroupe(String nom) {
	  this.groupes.add(new Groupe(nom));
	}
	public void addGroupe(Groupe groupe){
		this.groupes.add(groupe);
	}
	public void addSujet(String titre) {
	  this.sujets.add(new Sujet(titre));
	}
	public void addSujet(Sujet sujet) {
		this.sujets.add(sujet);
	}
	public int getNbGroupe() {
	  return this.groupes.size();
	}
	public int getNbSujet() {
	  return this.sujets.size();
	}
	public Groupe getGroupe(int id) {
	  if(id >= 0 && id < this.getNbGroupe()) {
		return this.groupes.get(id);
	  }
	  return new Groupe("n'existe pas");
	}
	public void addProf(String prof) {
		this.profs.add(prof);
	}
	public ArrayList<Groupe> getGroupeAvecAffectation() {
		ArrayList<Groupe> groupesAff = new ArrayList<>();
		for(Groupe g: this.groupes) {
			if (g.getAffectation() != null) {
				groupesAff.add(g);
			}
		}
		return groupesAff;
	}
	public ArrayList<Groupe> getGroupeSansAffectation() {
		ArrayList<Groupe> groupesSansAff = new ArrayList<>();
		for(Groupe g: this.groupes) {
			if (g.getAffectation() == null) {
				groupesSansAff.add(g);
			}
		}
		return groupesSansAff;
	}

	//méthodes
	/**
	* toString
	* @return String instance bien formée
	*/
	public String toString() {
	  String str = "Liste des groupes:\n----------------\n";
	  for(int i=0; i<this.groupes.size(); i++) {
		str += this.groupes.get(i).toString() + "\n";
	  }
	  str += "\n\nListe des sujets:\n------------------\n";
	  for(int i=0; i<this.sujets.size(); i++) {
		str += this.sujets.get(i).toString() + "\n";
	  }
	  return str;
	}
	/**
	* serialize les groupes
	* enregistre le résultat dans target/groupes.json
	*/
	public void serializeGroupe() {
	  ObjectMapper test = new ObjectMapper();
	  try {
		test.writeValue(new File("target/groupes.json"), this.groupes);
	  } catch (Exception e) {
		e.printStackTrace();
	  }
	}
	/**
	* serialize les groupes
	* enregistre le résultat dans target/sujets.json
	*/
	public void serializeSujet() {
	  ObjectMapper test = new ObjectMapper();
	  try {
		test.writeValue(new File("target/sujets.json"), this.sujets);
	  } catch (Exception e) {
		e.printStackTrace();
	  }
	}
	/**
	* importe les groupes depuis target/groupes.json
	* les deserialize et écrase les groupes actuels dans gestionTer
	*/
	public void importGroupe() {
	  ObjectMapper test = new ObjectMapper();
	  try {
		Groupe[] groupesS = test.readValue(new File("target/groupes.json"), Groupe[].class);
		this.groupes = new ArrayList<>();
		for(int i=0; i<groupesS.length; i++) {
		  this.groupes.add(groupesS[i]);
		}
	  } catch (Exception e) {
		e.printStackTrace();
	  }
	}
	/**
	* importe les sujets depuis target/sujets.json
	* les deserialize et écrase les groupes actuels dans gestionTer
	*/
	public void importSujet() {
	  ObjectMapper test = new ObjectMapper();
	  try {
		Sujet[] sujetsS = test.readValue(new File("target/sujets.json"), Sujet[].class);
		this.sujets = new ArrayList<>();
		for(int i=0; i<sujetsS.length; i++) {
		  this.sujets.add(sujetsS[i]);
		}
	  } catch (Exception e) {
		e.printStackTrace();
	  }
	}
	/**
	* génère une liste d'instances de PlanningCombinaison, avec pour chaque:
	* un groupe, une date, un professeur
	* @return ArrayList<PlanningCombinaison> la liste du planning généré
	*/
	public ArrayList<PlanningCombinaison> genererPlanning() {
		ArrayList<PlanningCombinaison> planning = new ArrayList<>();
		ArrayList<Groupe> groupesAff = this.getGroupeAvecAffectation();
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
		LocalDateTime horaire = LocalDateTime.parse("2021/12/10 08:00", format);
		final int dureeSoutenance = 45;
		for(Groupe g: groupesAff) {
			PlanningCombinaison plan = new PlanningCombinaison();
			plan.setGroupe(g);
			plan.setProf(g.getProf());
			plan.setDate(horaire);
			planning.add(plan);
			horaire.plusMinutes(dureeSoutenance);
			//il faudrait pouvoir passer au jour suivant, si l'heure dépasse une heure max
		}
		return planning;
	}
	/**
	* crée la liste d'adjacence qui sera utilisée par le hongrois
	* chaque ligne est un groupe
	* chaque colonne est un sujet
	* à la case (groupe, sujet), l'ordre de priorité du voeu correspondant
	* (10000 si voeu non effectué sur ce sujet)
	* @return matrice d'adjacence
	*/
	public List<List<Integer>> creerMatrice() {
		List<List<Integer>> matrice = new ArrayList<List<Integer>>();
		for(Groupe g: this.getGroupeSansAffectation()) {
			List<Integer> ligne = new ArrayList<Integer>();
			for(Sujet s: this.getSujets()) {
				int ordre = g.getOrdreSujet(s);
				if (ordre == -1) {
					ordre = 10000;
					//un sujet non choisi est un sujet qu'on ne veut pas que le hongrois choisisse
				}
				ligne.add(ordre);
			}
			matrice.add(ligne);
		}
		return matrice;
	}

	/**
	* paire les résultats du hongrois, de chaque groupe à chaque sujets
	* @param matrice List<List<Integer>>
	* @return boolean true si l'affectation s'est déroulée correctement
	*/
	public boolean opererAffectation(List<List<Integer>> matrice) {
		//à effectuer
		return false;
	}

	/**
	* lance l'affectation
	* @param phase int numéro de la phase
	* @return boolean true si l'affectation s'est déroulée correctement
	*/
	public boolean lancerAffectation(int phase) throws AffectationImpossibleException, TailleInvalideException {
		int hauteur = this.getNbGroupe();
		int largeur = this.getNbSujet();
		hongrois.setHauteur(hauteur);
		hongrois.setLargeur(largeur);
		List<List<Integer>> matrice = this.creerMatrice();
		hongrois.setAdjacenceList(matrice);
		List<List<Integer>> resultat = hongrois.affectation(phase);
		if(!this.opererAffectation(resultat)) {
			throw new AffectationImpossibleException("Resultat du hongrois incongru");
		}
		return true;
	}
}